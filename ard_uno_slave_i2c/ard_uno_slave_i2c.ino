/*
 * Arduino slave for Raspberry Pi Master
 * i2c_slave
 */

//include the wire library for I2c
#include<Wire.h>

// LED on pin 13
const int ledPin = 13;

void setup() {
  // join I2C bus as slave
  Wire.begin(0x8);

  // call receiveEnent when data is received
  Wire.onReceive(receiveEvent);

  // Setup pin 13 as output and turn on and off the LED
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, LOW);

}

// Function that executes whenever data is received from Master
void receiveEvent(int howMany){

  while(Wire.available()){ // Loop through all the available pins
    char c = Wire.read(); // receive byte as a character
    digitalWrite(ledPin, c);
  }
  
}

void loop() {
  delay(100);

}
