from smbus import SMBus # This is the library we need to work with i2c

addr = 0x8 # bus address. This is the address of our slave
bus = SMBus(1) # The 1 in braces indicates we will be using /dev/i2c-1

numb = 1

print("Enter 1 for on and 0 for of")

while numb == 1:
	
	ledstate = input(">>>>	")
	
	if ledstate == "1":
		bus.write_byte(addr, 0x1) # switch on the led
	elif ledstate == "0":
		bus.write_byte(addr, 0x0) # switch of the led
	else:
		numb = 0
